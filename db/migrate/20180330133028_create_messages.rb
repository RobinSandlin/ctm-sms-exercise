class CreateMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :messages do |t|
      t.string :sender_name
      t.string :phone
      t.text :body

      t.timestamps
    end
    add_index :messages, :sender_name
  end
end
