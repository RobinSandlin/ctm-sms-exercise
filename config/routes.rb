Rails.application.routes.draw do
  resources :messages

  post '/:integration_name' => 'webhooks#receive', as: :receive_webhooks

  # constraints subdomain: 'hooks' do
  #   post '/:integration_name' => 'webhooks#receive', as: :receive_webhooks
  # end


  root 'messages#new'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
