class Message < ApplicationRecord
    PHONE_REGEX = /^(\+1)?\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/

    before_validation :strip_phone

    validates :sender_name, :length => { :minimum => 3 }
    validates :phone, :length => { :minimum => 7 }, format: { with: /(\+?1?\d{3}-?\d{3}-?\d{4})/, message: "bad format in phone number" }
    before_save :clean_phone

    def strip_phone
        self.phone = self.phone.to_s.strip
    end

    def clean_phone
        self.phone = ActionController::Base.helpers.number_to_phone(self.phone.to_s.gsub(/[^0-9+]/, ''))
    end

    def sms_send
        account_sid = 'AC02db1aa53f22a8be4c4a474b0dcaca5c'
        auth_token = 'caefa5304fc5dd1b1087920d79b66fde'
        twilio_number = '+14437753675'
        client = Twilio::REST::Client.new(account_sid, auth_token)

        client.messages.create(
            to: self.phone,
            from: twilio_number,
            body: self.body)
    end
end
