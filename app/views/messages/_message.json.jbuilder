json.extract! message, :id, :sender_name, :phone, :body, :created_at, :updated_at
json.url message_url(message, format: :json)
