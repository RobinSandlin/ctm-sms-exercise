module ApplicationHelper
    def message_datetime(d)
        d.localtime.strftime('%l:%M:%S %p  %B %e, %Y')
    end
end
