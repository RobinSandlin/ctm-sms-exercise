class WebhooksController < ApplicationController
  skip_before_action :verify_authenticity_token

  def receive
    # <?xml version="1.0" encoding="UTF-8"?>
    # <Response>
    #     <Say>Thanks for calling!</Say>
    # </Response>

    begin
      if request.headers['Content-Type'] == 'application/json'
        js = JSON.parse(request.body.read)
      elsif request.headers['Content-Type'] == 'text/xml' # or 'text/html'?
        js = Hash.from_xml(request.body.read).to_json
      else
        # application/x-www-form-urlencoded
        js = params.as_json
      end


      if Rails.env.development?
        logger.debug request.headers['Content-Type']
        logger.debug "Params Sent: #{js.to_s}"
      end

      @message = Message.new(json_to_message_params(js))
      if @message.save()
        head :ok
      else
        logger.info "webhooks#receive rejected sms message from provider:"
        logger.info "Params Sent: #{js.to_s}"
        logger.info "Message parameters: #{json_to_message_params(js).to_s}"

        head :unprocessable_entity
      end

    rescue Exception => e
      puts e.message
      head :unprocessable_entity
    end
  end

  def json_to_message_params(js)
    p = {}
    p[:sender_name] = js['From']
    p[:phone] = js['To']
    p[:body] = js['Body']
    p
  end
end
